﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Initialization" Type="Folder">
		<Item Name="Check if Directories Exist.vi" Type="VI" URL="../Support/Check if Directories Exist.vi"/>
		<Item Name="Start System Parameters.vi" Type="VI" URL="../Initializations/Start System Parameters.vi"/>
	</Item>
	<Item Name="Variables" Type="Folder">
		<Item Name="Parent" Type="Folder">
			<Item Name="Variable.lvclass" Type="LVClass" URL="../Variables/Parent/Variable.lvclass"/>
		</Item>
		<Item Name="Manager" Type="Folder">
			<Item Name="Initialize Variables.vi" Type="VI" URL="../Variables/Manager/Initialize Variables.vi"/>
			<Item Name="Variable Settings.vi" Type="VI" URL="../Variables/Manager/Variable Settings.vi"/>
			<Item Name="Save Variables.vi" Type="VI" URL="../Variables/Manager/Save Variables.vi"/>
			<Item Name="Get Variable Data.vi" Type="VI" URL="../Variables/Manager/Get Variable Data.vi"/>
		</Item>
		<Item Name="Methods" Type="Folder">
			<Item Name="Get Variable By Name.vi" Type="VI" URL="../Variables/Methods/Get Variable By Name.vi"/>
		</Item>
		<Item Name="Children" Type="Folder"/>
	</Item>
	<Item Name="Dialogs" Type="Folder">
		<Item Name="Explorer" Type="Folder">
			<Item Name="Dialogs" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Create test.vi" Type="VI" URL="../Dialogs/Explorer/Dialogs/Create test.vi"/>
				<Item Name="Create System Config.vi" Type="VI" URL="../Dialogs/Explorer/Dialogs/Create System Config.vi"/>
				<Item Name="Create Program Folder.vi" Type="VI" URL="../Dialogs/Explorer/Dialogs/Create Program Folder.vi"/>
			</Item>
			<Item Name="Support" Type="Folder">
				<Item Name="Open Test Editor.vi" Type="VI" URL="../Dialogs/Explorer/Support/Open Test Editor.vi"/>
				<Item Name="Remove tree file extensions.vi" Type="VI" URL="../Support/File Tree Explorer/Remove tree file extensions.vi"/>
				<Item Name="Rename File.vi" Type="VI" URL="../Dialogs/Explorer/Support/Rename File.vi"/>
			</Item>
			<Item Name="File Explorer.vi" Type="VI" URL="../Dialogs/Explorer/File Explorer.vi"/>
		</Item>
		<Item Name="System File Editor" Type="Folder">
			<Item Name="Support" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Menu.rtm" Type="Document" URL="../Dialogs/System Editor/Support/Menu.rtm"/>
				<Item Name="Update System Editor Tree.vi" Type="VI" URL="../Dialogs/System Editor/Support/Update System Editor Tree.vi"/>
				<Item Name="System File Tree Menu Item Manager.vi" Type="VI" URL="../Dialogs/System Editor/Support/System File Tree Menu Item Manager.vi"/>
				<Item Name="Compare System Files.vi" Type="VI" URL="../Dialogs/System Editor/Support/Compare System Files.vi"/>
			</Item>
			<Item Name="System Editor.vi" Type="VI" URL="../Dialogs/Explorer/Dialogs/System Editor.vi"/>
		</Item>
		<Item Name="Support" Type="Folder">
			<Item Name="Check and add test and system parameters to queues.vi" Type="VI" URL="../Dialogs/Support/Check and add test and system parameters to queues.vi"/>
			<Item Name="Get Tests.vi" Type="VI" URL="../Dialogs/Support/Get Tests.vi"/>
			<Item Name="Set Config Select Defaults.vi" Type="VI" URL="../Dialogs/Support/Set Config Select Defaults.vi"/>
		</Item>
		<Item Name="Select Configuration" Type="Folder">
			<Item Name="Config Selector.vi" Type="VI" URL="../Dialogs/Config Selector.vi"/>
			<Item Name="Remove Tests from Tree.vi" Type="VI" URL="../Dialogs/Select Configuration/Remove Tests from Tree.vi"/>
		</Item>
		<Item Name="Settings" Type="Folder">
			<Item Name="Settings Dialog.vi" Type="VI" URL="../Dialogs/Settings/Settings Dialog.vi"/>
		</Item>
	</Item>
	<Item Name="Automatic Mode" Type="Folder">
		<Item Name="Support" Type="Folder">
			<Item Name="Automatic Menu.rtm" Type="Document" URL="../Automatic Mode/Support/Automatic Menu.rtm"/>
			<Item Name="Launch Auto.vi" Type="VI" URL="../Automatic Mode/Support/Launch Auto.vi"/>
		</Item>
		<Item Name="Methods" Type="Folder">
			<Item Name="Event Handlers" Type="Folder">
				<Item Name="Initialize UI.vi" Type="VI" URL="../Automatic Mode/Methods/Initialize UI.vi"/>
				<Item Name="Panel Resize.vi" Type="VI" URL="../Automatic Mode/Methods/Event Handlers/Panel Resize.vi"/>
			</Item>
		</Item>
		<Item Name="Type Def" Type="Folder">
			<Item Name="Control Refs.ctl" Type="VI" URL="../Automatic Mode/Type Defs/Control Refs.ctl"/>
		</Item>
		<Item Name="Automatic Main.vi" Type="VI" URL="../Automatic Mode/Automatic Main.vi"/>
	</Item>
	<Item Name="Manual Mode" Type="Folder">
		<Item Name="Support" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Dialogs" Type="Folder">
				<Item Name="Launch Controller Status By Name.vi" Type="VI" URL="../Manual Mode/Support/Dialogs/Launch Controller Status By Name.vi"/>
				<Item Name="Launch DAQ Status By Name.vi" Type="VI" URL="../Manual Mode/Support/Dialogs/Launch DAQ Status By Name.vi"/>
			</Item>
			<Item Name="VI Panel Functions" Type="Folder">
				<Item Name="Intialize VI.vi" Type="VI" URL="../Manual Mode/Support/Intialize VI.vi"/>
				<Item Name="Manual Panel Resize.vi" Type="VI" URL="../Manual Mode/Support/VI Panel Functions/Manual Panel Resize.vi"/>
				<Item Name="Limit Trip Dialog.vi" Type="VI" URL="../Manual Mode/Support/VI Panel Functions/Limit Trip Dialog.vi"/>
			</Item>
			<Item Name="Check if valid Input.vi" Type="VI" URL="../Manual Mode/Support/Check if valid Input.vi"/>
			<Item Name="Controls Manager.vi" Type="VI" URL="../Manual Mode/Support/Controls Manager.vi"/>
			<Item Name="Recorders Manager.vi" Type="VI" URL="../Manual Mode/Support/Recorders Manager.vi"/>
			<Item Name="Create Manual Queues.vi" Type="VI" URL="../Manual Mode/Support/Create Manual Queues.vi"/>
			<Item Name="Destroy Manual Queues.vi" Type="VI" URL="../Manual Mode/Support/Destroy Manual Queues.vi"/>
			<Item Name="Launch Manual Mode.vi" Type="VI" URL="../Manual Mode/Support/Launch Manual Mode.vi"/>
			<Item Name="Build Setpoints.vi" Type="VI" URL="../Manual Mode/Support/Build Setpoints.vi"/>
			<Item Name="Send Setpoints.vi" Type="VI" URL="../Manual Mode/Support/Send Setpoints.vi"/>
			<Item Name="Stop System Parameters.vi" Type="VI" URL="../Support/Stop System Parameters.vi"/>
			<Item Name="MenuBar.rtm" Type="Document" URL="../Manual Mode/MenuBar.rtm"/>
			<Item Name="Get max string width.vi" Type="VI" URL="../Manual Mode/Support/Get max string width.vi"/>
			<Item Name="Soft Stop Controllers.vi" Type="VI" URL="../Manual Mode/Support/Soft Stop Controllers.vi"/>
			<Item Name="Get Controller Info.vi" Type="VI" URL="../Manual Mode/Support/Get Controller Info.vi"/>
			<Item Name="Recorders Value Change Event.vi" Type="VI" URL="../Manual Mode/Support/Recorder Manager/Recorders Value Change Event.vi"/>
		</Item>
		<Item Name="Type Defs" Type="Folder">
			<Item Name="Manual Mode Queues.ctl" Type="VI" URL="../Manual Mode/Type Defs/Manual Mode Queues.ctl"/>
			<Item Name="Recorders Interface.ctl" Type="VI" URL="../Manual Mode/Type Defs/Recorders Interface.ctl"/>
			<Item Name="Manual main VI Refs.ctl" Type="VI" URL="../Manual Mode/Type Defs/Manual main VI Refs.ctl"/>
			<Item Name="Setpoints.ctl" Type="VI" URL="../Manual Mode/Type Defs/Setpoints.ctl"/>
		</Item>
		<Item Name="Manual Main.vi" Type="VI" URL="../Manual Mode/Manual Main.vi"/>
	</Item>
	<Item Name="Support" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="Apply System Parameters to Test.vi" Type="VI" URL="../Support/Apply System Parameters to Test.vi"/>
		<Item Name="Apply Test to System Parameters.vi" Type="VI" URL="../Support/Apply Test to System Parameters.vi"/>
		<Item Name="Compare System and Test.vi" Type="VI" URL="../Support/Compare System and Test.vi"/>
		<Item Name="Create Limit Trip Screenshot File Path.vi" Type="VI" URL="../Support/Create Limit Trip Screenshot File Path.vi"/>
		<Item Name="Get Channel Names.vi" Type="VI" URL="../Support/Get Channel Names.vi"/>
		<Item Name="Get Controllers Setpoints.vi" Type="VI" URL="../Support/Get Controllers Setpoints.vi"/>
		<Item Name="Load System Parameters.vi" Type="VI" URL="../Support/Load System Parameters.vi"/>
		<Item Name="Load Test.vi" Type="VI" URL="../Support/Load Test.vi"/>
		<Item Name="Save System Parameters.vi" Type="VI" URL="../Support/Save System Parameters.vi"/>
		<Item Name="Start Controllers.vi" Type="VI" URL="../Support/Start Controllers.vi"/>
		<Item Name="Start DAQ.vi" Type="VI" URL="../Support/Start DAQ.vi"/>
		<Item Name="Start Limits.vi" Type="VI" URL="../Support/Start Limits.vi"/>
		<Item Name="Start Recorders.vi" Type="VI" URL="../Support/Start Recorders.vi"/>
		<Item Name="Stop Controllers.vi" Type="VI" URL="../Support/Stop Controllers.vi"/>
		<Item Name="Stop DAQ.vi" Type="VI" URL="../Support/Stop DAQ.vi"/>
		<Item Name="Stop Recorders.vi" Type="VI" URL="../Support/Stop Recorders.vi"/>
	</Item>
	<Item Name="Type Defs" Type="Folder">
		<Item Name="Application Operating Parameters.ctl" Type="VI" URL="../Type Defs/Application Operating Parameters.ctl"/>
		<Item Name="System Parameters.ctl" Type="VI" URL="../Type Defs/System Parameters.ctl"/>
		<Item Name="Display Type.ctl" Type="VI" URL="../Type Defs/Display Type.ctl"/>
	</Item>
	<Item Name="DAQ Manager" Type="Folder">
		<Item Name="Start DAQ Manager.vi" Type="VI" URL="../Data Manager/Start DAQ Manager.vi"/>
		<Item Name="Run DAQ Manager.vi" Type="VI" URL="../Data Manager/Run DAQ Manager.vi"/>
	</Item>
	<Item Name="Data Management" Type="Folder">
		<Item Name="Type Defs" Type="Folder">
			<Item Name="Loaded Data.ctl" Type="VI" URL="../Data Management/Type Defs/Loaded Data.ctl"/>
			<Item Name="Export Settings.ctl" Type="VI" URL="../Data Management/Type Defs/Export Settings.ctl"/>
		</Item>
		<Item Name="Export Types" Type="Folder">
			<Item Name="CSV Export Methods" Type="Folder">
				<Item Name="Create Header.vi" Type="VI" URL="../Data Management/Export Types/CSV Export Methods/Create Header.vi"/>
				<Item Name="Merge Data Sets.vi" Type="VI" URL="../Data Management/Export Types/CSV Export Methods/Merge Data Sets.vi"/>
				<Item Name="Resample Data and Add Time Channel.vi" Type="VI" URL="../Data Management/Export Types/CSV Export Methods/Resample Data and Add Time Channel.vi"/>
			</Item>
			<Item Name="CSV Export.vi" Type="VI" URL="../Data Management/Export Types/CSV Export.vi"/>
		</Item>
		<Item Name="Methods" Type="Folder">
			<Item Name="Launch Data Manager.vi" Type="VI" URL="../Data Management/Launch Data Manager.vi"/>
			<Item Name="Load Data.vi" Type="VI" URL="../Data Management/Load Data.vi"/>
		</Item>
		<Item Name="Data Manager.vi" Type="VI" URL="../Data Management/Data Manager.vi"/>
	</Item>
	<Item Name="Mclaren Dyno Program.vi" Type="VI" URL="../Mclaren Dyno Program.vi"/>
</Library>
